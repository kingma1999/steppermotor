#include "StepperMotor.h"

// Initialize the steppermotor on the connected pins, and set those pins as outputs
StepperMotor::StepperMotor(int pwm, int dir, int state1, int state2) {
    pwmPin = pwm;
    dirPin = dir;
    statePin1 = state1;
    statePin2 = state2;
    pinMode(pwmPin, OUTPUT);
    pinMode(dirPin, OUTPUT);
    pinMode(statePin1, OUTPUT);
    pinMode(statePin2, OUTPUT);
}

// Simple getter function
int StepperMotor::getTargetDelay() {
    return targetDelay;
}

// Function to set the direction by either setting dirPin HIGH or LOW, this controls the EasyDriver
void StepperMotor::setDirection(int dir) {
    if (dir > 0) {
        digitalWrite(dirPin, LOW);
    }
    else {
        digitalWrite(dirPin, HIGH);
    }
}

// StatePin 1 and 2 are used together to encode 4 different step sizes on the EasyDriver: Full, half, quarter and one/eight steps sizes
void StepperMotor::setStepMode(stepSize step) {
    switch (step) {
    case Full:
        digitalWrite(statePin1, LOW);
        digitalWrite(statePin2, LOW);
        break;
    case Half:
        digitalWrite(statePin1, HIGH);
        digitalWrite(statePin2, LOW);
        break;
    case Quarter:
        digitalWrite(statePin1, LOW);
        digitalWrite(statePin2, HIGH);
        break;
    case Eight:
        digitalWrite(statePin1, HIGH);
        digitalWrite(statePin2, HIGH);
        break;
    default:
        break;
    }
}

// function that sets the direction, and calculates the amount of steps to move from the amount of millimetres to move, and keeps track of its position
void StepperMotor::moveMM(int mmToMove) {
    setDirection(mmToMove);
    int steps = mmToMove * 25;
    rotate(abs(steps));
    positionMM = positionMM + mmToMove;
}

// function to rotate the leadscrew, if there are enough steps, the rotate function will use a sinusoid to accelerate and decellerate the leadscrew, as this allows for higher rotation speeds
void StepperMotor::rotate(int steps) {
    int startStopSteps = 0;
    int waitDelay = 1500;
    if (steps > 2000) {
        startStopSteps = 600;
        targetDelay = 1200;
    }
    else if (steps > 1000) {
        startStopSteps = 400;
        targetDelay = 1350;
    }
    else {
        startStopSteps = 0;
        targetDelay = 1500;
    }

    // The used sinusoids arent time dependant, just step dependant, making sure the exact amount of steps are set
    for (int i = 0; i < steps; i++) {
        if (i < startStopSteps) {
            waitDelay = ((stoppingDelay - getTargetDelay()) * 0.5) * cos((PI * i)/ (startStopSteps)) + (stoppingDelay + getTargetDelay()) / 2;
            Serial.println(waitDelay);
        }
        else if (i > steps - startStopSteps) {
            waitDelay = -((stoppingDelay - getTargetDelay()) * 0.5) * cos((PI * (i - (steps - startStopSteps) + 1))/ (startStopSteps)) + (stoppingDelay + getTargetDelay()) / 2;
            Serial.println(waitDelay);
        }
        else {
            waitDelay = getTargetDelay();
            Serial.println(waitDelay);
        }

        // Generate rising edge every 2 * waitdelay
        digitalWrite(pwmPin, HIGH);
        delayMicroseconds(waitDelay);
        digitalWrite(pwmPin, LOW);
        delayMicroseconds(waitDelay);
    }
    // Delay to make sure there isn't another rotation immediately afterwards
    delay(200);
}

// function that calculates the amount of millimetre to move based on its own position and the targeted column
void StepperMotor::toColumn(int column) {
    int extraMinus = 0;
    if (column >= 0 && column < 7) {
        if (positionColumn == 0) {
            extraMinus = -4;
        }
        int rotateMM = column * 45 + extraMinus - positionMM;
        moveMM(rotateMM);
        Serial.println(rotateMM);
        positionColumn = column;
    }
}