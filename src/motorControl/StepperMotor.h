#ifndef STEPPERMOTOR_H
#define STEPPERMOTOR_H

#include <Arduino.h>

class StepperMotor {
private:
    int pwmPin = 13;
    int dirPin = 12;
    int statePin1 = 14;
    int statePin2 = 27;
    int targetDelay = 1500;
    int stoppingDelay = 2500;
    int positionMM = 0;
    int positionColumn = 0;
public:
    StepperMotor(int pwm, int dir, int state1, int state2);
    enum stepSize {Full, Half, Quarter, Eight};
    int getTargetDelay();
    void setDirection(int dir);
    void setStepMode(stepSize step);
    void moveMM(int mmToMove);
    void rotate(int steps);
    void toColumn(int column);
    int getColumn();
};

#endif