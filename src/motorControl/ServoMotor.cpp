#include "ServoMotor.h"

// Wrapper class, used for easy initialization
ServoMotor::ServoMotor(int data) {
    dataPin = data;
    servo.attach(data);
}

// Wrapper function to rotate the servo
void ServoMotor::rotate(int degrees) {
    servo.write(degrees);
}

// function to rotate from retract angle to push angle at a certain delay per degree rotated
void ServoMotor::rotateSpeedControl(int waitDelay) {
    for (int i = retractAngle; i <= pushAngle; i++) {
        rotate(i);
        delay(waitDelay);
    }
}

// function to slowly eject a chip, wait a little, and then rotate back
void ServoMotor::eject() {
    rotateSpeedControl(10);
    delay(100);
    rotate(retractAngle);
}

// Wrapper function to read the current angle
int ServoMotor::read() {
    return servo.read();
}