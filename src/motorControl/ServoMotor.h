#ifndef SERVOMOTOR_H
#define SERVOMOTOR_H

#include <Arduino.h>
#include <ESP32Servo.h>

class ServoMotor {
private:
    int dataPin = 0;
    Servo servo;
    int pushAngle = 130;
    int retractAngle = 35;
public:
    ServoMotor(int data);
    void rotate(int degrees);
    void rotateSpeedControl(int waitDelay);
    void eject();
    int read();
};

#endif